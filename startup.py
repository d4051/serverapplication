import os
from multiprocessing import Process
from time import sleep
from mdns import RegisterService,UnregisterService
from multicast_receiver import close_multicast_socket, init_multicast_receiver,get_multicast_data
from loadbalancer import init_loadbalancer,get_loadbalancer_data
import atexit
from pymongo import MongoClient
from pprint import pprint
from bson.json_util import dumps, loads
import socket
from Server import run_server,init_server
import json

REGISTER_COURSE = "register_course"
DEREGISTER_COURSE = "deregister_course"
GET_MY_COURSES = "get_my_course"
ADD_COURSE = "add_course"
VIEW_COURSE_DETAILS = "view_course_details"
GET_AVAILABLE_COURSES = "get_available_courses"

def multicast_receiver_process():
    print('multicast_receiver_process ')
    print('Parent process id:', os.getppid())
    print('Process id:', os.getpid())
    init_multicast_receiver()
    # connect to MongoDB
    client = MongoClient(port=27017)
    db=client.CourseRegistration

    try:
        while True:
                data = get_multicast_data()
                if data:
                    data = data['data']
                    if data['request'] == REGISTER_COURSE:
                        course = {
                        "id" : data['id'],
                        "user_type" : data['user_type'],
                        "course_name" : data['course_name'],
                        "course_id" : data['course_id'],
                        "course_type" : data['course_type'],
                        "course_credits" : data['course_credits'],
                        }
                        result=db.RegistrationDetails.insert_one(course)
                        if (result.acknowledged):
                            registered_students = db.RegistrationDetails.count_documents({'course_id': data['course_id']})
                        
                            filter = {'course_id': data['course_id']}
                            newvalues = { "$set": { 'no_of_students': registered_students } }

                            # Update student count to course details.
                            db.CourseDetails.update_one(filter,newvalues)

                    elif  data['request'] == DEREGISTER_COURSE:
                            course = {
                            "id" : data['id'],
                            "user_type" : data['user_type'],
                            "course_name" : data['course_name'],
                            "course_id" : data['course_id'],
                            "course_type" : data['course_type'],
                            "course_credits" : data['course_credits'],
                            }
                            result=db.RegistrationDetails.delete_one({'id': data['id'],'course_id': data['course_id']})

                            if (result.acknowledged):
                                registered_students = db.RegistrationDetails.count_documents({'course_id': data['course_id']})
                            
                                filter = {'course_id': data['course_id']}
                                newvalues = { "$set": { 'no_of_students': registered_students } }

                                # Update student count to course details.
                                db.CourseDetails.update_one(filter,newvalues)   

                    elif  data['request'] == ADD_COURSE:
                            course = {
                            "id" : data['id'],
                            "user_type" : data['user_type'],
                            "course_name" : data['course_name'],
                            "course_id" : data['course_id'],
                            "course_type" : data['course_type'],
                            "course_credits" : data['course_credits'],
                            "no_of_students" : 0
                            }
                            result=db.CourseDetails.insert_one(course)    
                    print("Multicast message")
                    print(data)

    except:
            close_multicast_socket()    


def loadbalancer_server_process(leader,group):    
    print('loadbalancer_process ')
    print('Parent process id:', os.getppid())
    print('Process id:', os.getpid())
    init_loadbalancer(leader,group)
    while True:
        get_loadbalancer_data()

def mdns_service_process():    
    print('mdns_service_process ')
    print('Parent process id:', os.getppid())
    print('Process id:', os.getpid())
    RegisterService()

def server_handler_process():    
    print('server_handler_process ')
    print('Parent process id:', os.getppid())
    print('Process id:', os.getpid())
    init_server()
    run_server()

def exit_handler():
    print("Application exit") 

def main():
    try:    

        atexit.register(exit_handler)
        
        server_process = Process(target=server_handler_process)
        server_process.start()

        # mdns_process.start()

       
        
        # multicast_process.start()
        
        # mdns_process.join()
        # loadbalancer_process.join()
        # multicast_process.join()
        # server_process.join()

         # Create a UDP socket
        comm_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        # Server application IP address and port
        comm_address = socket.gethostbyname(socket.gethostname())
        comm_port = 10004
        buffer_size = 1024
        comm_socket.bind((comm_address, comm_port))

        MY_HOST = socket.gethostname()
        MY_IP = socket.gethostbyname(MY_HOST)

        multicast_process = None
        loadbalancer_process = None
        mdns_process = None
        while True:
            try: 
                print('\nWaiting to receive communication message from serverleader...\n')
                data, addr = comm_socket.recvfrom(1024)
                print(data)
                if data:
                    buffer = json.loads(data)
                    if buffer['type'] == "updateGroup":
                        if(buffer['leader'] == MY_IP):

                            if multicast_process!= None:    
                                if multicast_process.is_alive():
                                    multicast_process.terminate()  
                                    multicast_process = None
                                    print("I am leader..Terminating multicast recceiver processes.")

                            print("I am leader :) Starting loadbalancer and mdns processes.")

                            loadbalancer_process = Process(target=loadbalancer_server_process,args=(buffer['leader'],buffer['group_view']))
                            loadbalancer_process.start()

                            mdns_process = Process(target=mdns_service_process)
                            mdns_process.start()
                        else:
                            print("I am not leader.. Start receiving multicast messages..")
                            multicast_process = Process(target=multicast_receiver_process)
                            multicast_process.start()

                            if loadbalancer_process!= None:
                                if loadbalancer_process.is_alive():
                                    loadbalancer_process.terminate()
                                    loadbalancer_process = None
                                    print("I am not leader..Terminating loadbalancer processes.")

                            if mdns_process!= None:
                                if mdns_process.is_alive():
                                    mdns_process.terminate() 
                                    mdns_process = None   
                                    print("I am not leader..Terminating mdns processes.")                      

                    print("Received message from serverleader")
                    print(buffer)
                sleep(0.1)
            except:
                pass

    except KeyboardInterrupt:
        pass
    finally:
        print("Closing Server...")
    

if __name__ == '__main__':
    main()


        
