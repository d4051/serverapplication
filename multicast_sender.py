import socket
import json

MULTICAST_GRP = '224.0.0.1'
MULTICAST_PORT = 4004

MULTICAST_TTL = 2

def write_global_count(seq):
    file1 = open("MyFile.txt","w+")
    file1.write(str(float(seq)))
    file1.close()

def get_global_count():
    file1 = open("MyFile.txt","r+")
    cnt = file1.read()
    file1.close()
    return int(float(cnt))

def create_multicast_sender():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, MULTICAST_TTL)
    return sock

def send_multicast(sock,broadcast_message,total_messages):
    data = {
        "total_messages" : total_messages,
        "data" : broadcast_message
    }
    write_global_count(total_messages)
    sock.sendto(json.dumps(data).encode(), (MULTICAST_GRP, MULTICAST_PORT))
    # data, addr = sock.recvfrom(1024)
    # print (data)

sock = create_multicast_sender()


