import re
import socket
import struct
import json
from multicast_sender import create_multicast_sender,send_multicast,get_global_count

MULTICAST_GRP = '224.0.0.1'
MULTICAST_PORT = 4004
ONLY_MULTICAST_GROUP = False

multicast_sender_socket = create_multicast_sender()


# def sendResponse(addr, data):
#     message = str(data)
#     sock.sendto(str.encode(message), addr)

def close_multicast_socket():
    print("Terminating multicast receiver socket")
    sock.close()

def init_multicast_receiver():
    global sequence_latest_message 
    sequence_latest_message = get_global_count()

    print("Global Count for multicast " + str(sequence_latest_message))

    global last_received_message 
    last_received_message = 0

    global holdback_queue 
    holdback_queue = []
    global delivery_queue 
    delivery_queue = []

    global sock
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    if ONLY_MULTICAST_GROUP:
        sock.bind((MULTICAST_GRP, MULTICAST_PORT))
    else:
        sock.bind(('', MULTICAST_PORT))

    mreq = struct.pack("4sl", socket.inet_aton(MULTICAST_GRP), socket.INADDR_ANY)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

    print("Starting FIFO multicast")

    return sock

def get_multicast_data():    
    global sequence_latest_message
    global last_received_message 

    if delivery_queue :
        return delivery_queue.pop()

    try:
        data, addr = sock.recvfrom(1024)
        buffer = json.loads(data)
        if(buffer['total_messages'] == sequence_latest_message + 1):
            # print(buffer)
            ## deliver the message
            sequence_latest_message = sequence_latest_message  + 1
            delivery_queue.append(buffer)

            ## Send multicast again for reliablility
            send_multicast(multicast_sender_socket,buffer['data'],buffer['total_messages'])
            # Check if holdback queue contains any data.
            for item in holdback_queue:
                if(item['total_messages'] == sequence_latest_message + 1):
                    delivery_queue.append(item)
                    sequence_latest_message = sequence_latest_message  + 1
                    holdback_queue.pop()
            
        elif (buffer['total_messages'] > sequence_latest_message + 1):
            x = {
                'request' : "missed_packet",
                'message' : sequence_latest_message + 1
            } 
            # Request leader to resend the missing packet
            sock.sendto(json.dumps(x).encode(), addr)
            ## Send other messages first
            print("Store message to hold back queue")
            print(buffer)        
            print()
            holdback_queue.append(buffer)

        elif (buffer['total_messages'] < sequence_latest_message + 1):
            ## old message ->> Drop 
            print( "Message rejected " + str(buffer))
        # print(delivery_queue)
        
        if not delivery_queue:
            # print("List is empty")
            return
        else:
            print("Delivery Queue")
            print(delivery_queue)
            return delivery_queue.pop()

    except KeyboardInterrupt:
        pass

    # finally:
        # close_multicast_socket()
        
        #sendResponse(addr,"Message Received" + str(data))
