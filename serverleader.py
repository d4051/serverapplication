from time import sleep
import broadcast
import socket
import json
import lcrAlgorithm
import ipaddress
from multicast_sender import get_global_count
from registration_data import get_course_collection,get_registration_collection

if __name__ == '__main__':

    ##################### INFO ABOUT THE SERVER ####################

    # Local host information
    MY_HOST = socket.gethostname()
    MY_IP = socket.gethostbyname(MY_HOST)

    # Hardcoded for testing porpuses
    IP = MY_IP #"127.0.0.1"
    # IP = "127.0.0.1"
    PORT = 10000
    COMMPORT = 10002
    ELECTIONPORT = 10003
    MAINPROCESSPORT = 10004
    leader_uid = ""
    participant = False
    election_messageIP = {
        "mid": IP,
        "isLeader": False,
        "type": "election"
    }
    first_time = True
    initial_message = {
        "type": "new",
        "data": IP
    }
    # Get the initial group view
    # broadcast.deteleContent()
    # initial_group = broadcast.readFile()

    # The group view will be the list of servers in the system
    GROUP_VIEW = []

    BROADCAST_IP = '100.64.88.47'
    # Create a UDP socket for broadcasting
    send_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # Set the socket to broadcast and enable reusing addresses
    send_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    send_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Sends broadcast for some other server to hear
    send_socket.sendto(json.dumps(initial_message).encode(), (BROADCAST_IP, PORT))
    # send_socket.close()

    # Create a UDP socket for broadcasting
    listening_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # Set the socket to broadcast and enable reusing addresses
    listening_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    listening_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    listening_socket.bind(('', PORT))
    listening_socket.settimeout(1.0)

    
    # Create a UDP socket for group and communication
    # server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # # Set the socket to broadcast and enable reusing addresses
    # server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    # server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((IP, COMMPORT))

    # server_socket.bind((IP, COMMPORT))
    server_socket.settimeout(1.0)
    print('Server up and running at {}:{}'.format(IP, COMMPORT))

    # Create a UDP socket for election
    election_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # Set the socket to broadcast and enable reusing addresses
    election_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    election_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    election_socket.bind((IP, ELECTIONPORT))

    # Create a UDP socket for communication with main process
    main_process_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # Set the socket to broadcast and enable reusing addresses
    main_process_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    main_process_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # main_process_socket.bind((IP, MAINPROCESSPORT))    

    retry_leader_count = 0
    retry_server_count = 0
    retry_discovery_count = 0

    while True:
        if first_time:
            try: 
                data, addr = server_socket.recvfrom(1024)
                if(data):
                    new_group = json.loads(data.decode())
                    try:
                        GROUP_VIEW = new_group["ips"]
                        print("thank you for accepting me!!")
                        leader_uid = addr[0]
                        first_time = False
                        retry_discovery_count = 0
                        messageProcess = {
                            "type":"updateGroup",
                            "leader": leader_uid,
                            "group_view" : GROUP_VIEW
                        }
                        main_process_socket.sendto(json.dumps(messageProcess).encode(), (IP, MAINPROCESSPORT))
                    except:
                        print('Something went wrong with the first time thing')
                        pass
            except socket.timeout:
                    send_socket.sendto(json.dumps(initial_message).encode(), (BROADCAST_IP, PORT))
                    retry_discovery_count = retry_discovery_count + 1

            if  retry_discovery_count > 5:
                retry_discovery_count = 0
                print("Did not receive anything :( , therefore I am the leader :D")
                leader_uid = IP
                # broadcast.storeIP(IP)
                GROUP_VIEW.append(IP)
                first_time = False
                # main_process_socket.sendto(json.dumps(messageProcess).encode(), (IP, MAINPROCESSPORT))
                      
        
        print("This is leader: "+leader_uid)
            
        
        # The leader accepts for upcoming requests for a new server to join
        if(IP == leader_uid):
            try:
                data, addr = listening_socket.recvfrom(1024)
                if(data):
                    # On new request, it stores the IP on file and sends and sends the new to others
                    new_IP = json.loads(data.decode())
                    try:
                        if(new_IP["type"] == "new" and new_IP["data"]!=IP):
                            print("Received: " + new_IP["data"])
                            # broadcast.storeIP(new_IP["data"])
                            GROUP_VIEW.append(new_IP["data"])
                            # ips = broadcast.readFile()
                            ips = GROUP_VIEW
                            message = {"type":"discovery","ips":ips}
                            for ip in ips:
                                #if( ip != (IP + "\n")):
                                if( ip != IP):
                                    print("sending group to member: " + ip)
                                    #broadcast.sendResponse((ip[0:-1], PORT), json.dumps(message), server_socket)
                                    #server_socket.sendto(json.dumps(message).encode(),(ip[0:-1], COMMPORT))
                                    server_socket.sendto(json.dumps(message).encode(),(ip, COMMPORT))

                            buffer = {
                                'srcip' : '',
                                'sport' : 0,
                                'request' : 'update_course_collection',
                                'course_collection_data' : get_course_collection(),
                                'registration_collection_data' : get_registration_collection(),
                                'global_sequence_count' : get_global_count()
                            }        
                            ip = new_IP["data"]
                            print("NEW IP")
                            print(ip)
                            messageProcess = {
                                    "type":"updateGroup",
                                    "leader": IP,
                                    "group_view" : ips
                                }
                            main_process_socket.sendto(json.dumps(messageProcess).encode(), (IP, MAINPROCESSPORT))
                            if ip!= MY_IP:
                                server_socket.sendto(json.dumps(buffer).encode(),(ip, 9999))
                    except:
                        print('Something went wrong with sendign group listening for new servers')
                        pass
                        
            except socket.timeout:
                print("No other server wanting to join :/, lets continue :D")

            
            
            #Leader checks if every server is ok only if there is more than one
            #group_quantity = len(broadcast.readFile())
            group_quantity = len(GROUP_VIEW)
            if(group_quantity > 1):
                message_status = {"type":"status"}
                #for ip in broadcast.readFile():
                for ip in GROUP_VIEW:
                    #if( ip != (IP + "\n")):
                    if( ip != IP ):
                        #server_socket.sendto(json.dumps(message_status).encode(),(ip[0:-1], COMMPORT))
                        server_socket.sendto(json.dumps(message_status).encode(),(ip, COMMPORT))
                current_responses = []
                current_quantity_cycles = 0
                while(current_quantity_cycles != (group_quantity - 1)):
                    server_socket.settimeout(1.0)
                    try:
                        data, addr = server_socket.recvfrom(1024)
                        if(data):
                            data_server = json.loads(data.decode())
                            try:
                                if(data_server["type"] == "status"):
                                    ip = data_server["data"]
                                    current_responses.append(ip)
                                    print("thank you " + ip + " for your response")
                                    retry_server_count = 0
                                else: break
                            except:
                                pass    

                    except socket.timeout:
                        retry_server_count = retry_server_count + 1
                        print("did not received anything from one server")
                    current_quantity_cycles = current_quantity_cycles + 1

                if  retry_server_count > 5:
                    retry_server_count = 0
                    # If some servers crashed, leader will update the group view and send the new one
                    current_responses.append(IP)
                    print("current responses:")
                    print(current_responses)
                    if(len(current_responses) != group_quantity):
                        message_mod = {"type":"modification","ips":current_responses}
                        #broadcast.deteleContent()
                        GROUP_VIEW.clear()
                        GROUP_VIEW = current_responses
                        for ip in current_responses:
                            #broadcast.storeIP(ip)
                            if(ip != IP):
                                server_socket.sendto(json.dumps(message_mod).encode(),(ip, COMMPORT))

                        messageProcess = {
                                    "type":"updateGroup",
                                    "leader": IP,
                                    "group_view" : GROUP_VIEW
                                }        
                        main_process_socket.sendto(json.dumps(messageProcess).encode(), (IP, MAINPROCESSPORT))

        # If it is not leader, then waits to receive information if new server joined
        else:
            server_socket.settimeout(1.0)
            try:
                data, addr = server_socket.recvfrom(1024)
                if(data):
                    leader_mssg = json.loads(data.decode())
                    if(leader_mssg["type"] == "discovery"):
                        print("a new server joined, leader sent new group, will update")
                        #current_group = broadcast.readFile()
                        current_group = GROUP_VIEW
                        new_group = leader_mssg["ips"]

                        messageProcess = {
                            "type":"updateGroup",
                            "leader": leader_uid,
                            "group_view" : new_group
                        }
                        main_process_socket.sendto(json.dumps(messageProcess).encode(), (IP, MAINPROCESSPORT))

                        for ip in new_group:
                            if ip not in current_group:  
                                #broadcast.storeIP(ip[0:-1])
                                GROUP_VIEW.append(ip)
                    if(leader_mssg["type"] == "status"):
                        print("leader asked for status, will send IP")
                        message_server = {"type":"status", "data": IP}
                        server_socket.sendto(json.dumps(message_server).encode(), (leader_uid, COMMPORT))
                    if(leader_mssg["type"] == "modification"):
                        print("leader sent new group because of crash, will update")
                        new_group = leader_mssg["ips"]
                        #broadcast.deteleContent()
                        messageProcess = {
                            "type":"updateGroup",
                            "leader": leader_uid,
                            "group_view" : new_group
                        }
                        main_process_socket.sendto(json.dumps(messageProcess).encode(), (IP, MAINPROCESSPORT))
                        GROUP_VIEW.clear()
                        for ip in new_group:
                            #broadcast.storeIP(ip)
                            GROUP_VIEW.append(ip)
                    retry_leader_count = 0    

            except socket.timeout:
                retry_leader_count = retry_leader_count + 1
                print("No news from the leader..Retry")

            if (retry_leader_count > 5):
                retry_leader_count = 0
                print("No news from the leader!!!!!! HE IS DEAD!!!!!! Lets do an election :D!!!!!")
                #current_group = broadcast.readFile()
                current_group = GROUP_VIEW
                pastLeader = leader_uid
                #current_group.remove(leader_uid + "\n")
                current_group.remove(leader_uid)

                # Update the groupview in file without the leader
                print("Will update group without the leader, this is the new one:")
                print(current_group)
                #broadcast.deteleContent()
                # GROUP_VIEW.clear()
                # for ip in current_group:
                #     #broadcast.storeIP(ip)
                #     GROUP_VIEW.append(ip)

                print("Printing again the current group")
                print(current_group)
                group_with_n = []
                for ip in current_group:
                    group_with_n.append(ip+"\n")
                print("this group will be the one with the ring")
                print(group_with_n)
                # Forms and sorts the ring
                ring = lcrAlgorithm.form_ring(current_group)
                print("this is ring")
                print(ring)
                neighbour = lcrAlgorithm.get_neighbour(ring, IP, "left")
                #print("neighbour is: "+neighbour)
                print("this is neighbour")
                print(neighbour)
                # Send first message to the neighbour
                election_socket.sendto(json.dumps(election_messageIP).encode(), (neighbour, ELECTIONPORT)) 
                while True:
                    data, address = election_socket.recvfrom(1024)
                    election_message = json.loads(data.decode())
                    print(election_message)
                    print(address)
                    if(election_message["type"] == "election"):
                        if election_message['isLeader']==True:
                            leader_uid = election_message['mid']
                            # forward received election message to left neighbour
                            participant = False
                            # Does not send message to neighbour if it is the leader
                            if(leader_uid != neighbour):
                                election_socket.sendto(json.dumps(election_message).encode(), (neighbour, ELECTIONPORT))    
                            print("phase 1")
                            #time.sleep(3) 
                            break
                        if ipaddress.IPv4Address(election_message['mid']) < ipaddress.IPv4Address(IP) and not participant:
                            new_election_message = {
                                "mid": IP,
                                "isLeader": False,
                                "type": "election"
                            }
                            participant = True
                            # send received election message to left neighbour
                            election_socket.sendto(json.dumps(new_election_message).encode(), (neighbour, ELECTIONPORT))
                            print("phase 2")
                            #time.sleep(3) 
                        elif ipaddress.IPv4Address(election_message['mid']) > ipaddress.IPv4Address(IP):
                            # send received election message to left neighbour
                            participant = True
                            election_socket.sendto(json.dumps(election_message).encode(), (neighbour, ELECTIONPORT))
                            print("phase 3")
                            #time.sleep(3) 
                        elif election_message['mid'] == IP:
                            leader_uid = IP
                            new_election_message = {
                                "mid": IP,
                                "isLeader": True,
                                "type": "election"
                            }
                            # send new election message to left neighbour
                            participant = False
                            election_socket.sendto(json.dumps(new_election_message).encode(), (neighbour, ELECTIONPORT))
                            print("phase 4 and is leader")
                            #time.sleep(3) 
                            break
                            #return True
                        else:
                            break     
                    #initial_group = current_group
                    print("new leader is: " + leader_uid)
                    # The leader will always get one more message, so it prints it and continue 
                if(leader_uid == IP):
                    data_listener, addr_listener = election_socket.recvfrom(1024)
                    if(data_listener):
                        data_ip = data_listener.decode()
                        print("This is the extra message", data_ip)
                        messageProcess = {
                            "type":"updateGroup",
                            "leader": IP,
                            "group_view" : current_group
                        }
                        main_process_socket.sendto(json.dumps(messageProcess).encode(), (IP, MAINPROCESSPORT))
                    #continue
                #continue

                GROUP_VIEW = current_group



        

        print("back to beginning")




    