from ntpath import join
import sys
import signal
import logging
from socket import *
from itertools import cycle
import json

REQUEST_TYPE_WRITE = "write"
REQUEST_TYPE_READ = "read"
REGISTER_COURSE = "register_course"
DEREGISTER_COURSE = "deregister_course"
GET_MY_COURSES = "get_my_course"
ADD_COURSE = "add_course"
VIEW_COURSE_DETAILS = "view_course_details"
GET_AVAILABLE_COURSES = "get_available_courses"
# Load balancer port 
LB_PORT = 10001

# Servers in the network to which loadbalancer will route requests. 
# TODO : Server list should be updated dynamically.


# Route request using round robin algorithm.
def select_server():
    global ITER
    return round_robin(ITER)

def round_robin(iter):
    return next(iter)

def init_loadbalancer(leader,group):

    global SERVERS
    global LEADER
    global ITER

    SERVERS = []
    for item in group:
        SERVERS.append((item,9999))
    LEADER = (leader, 9999)
    ITER = cycle(SERVERS)
    # Create socket to talk with mobile client.
    global s1
    global s2
    try:
        s1 = socket(AF_INET, SOCK_DGRAM)
        s1.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        s1.bind(('', LB_PORT))

        # Create second socket to forward request to servers.
        s2 = socket(AF_INET, SOCK_DGRAM)
        s2.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)

        global run_loadbalancer 
        run_loadbalancer = True

        print ('Starting loadbalancer on port %d' % LB_PORT)

    except:
        logging.exception('')
        print('Exception')
        sys.exit(1)

# Close socket connections on signal interrupt. (On KeyboardInterrupt exception)
def close_loadbalancer():
    print("Terminating loadbalancer")
    global run_loadbalancer 
    run_loadbalancer = False
    s1.close()
    s2.close()


# register signal handler.     
# signal.signal(signal.SIGINT, signal_handler)


def get_loadbalancer_data():
    if(run_loadbalancer):            
        try:
            while True:

                # Receive data from mobile client.
                buf, (srcip, sport) = s1.recvfrom(65535)
                print(srcip)
                print(sport)
                
                #TODO Read or write decide
                # Forward data received from mobile client along with client IP and client port to server(selected from servers group) for further handling.
                
                buffer = (json.loads(buf))

                print(buffer)
                if buffer['request_type'] == REQUEST_TYPE_WRITE:
                    if buffer['request'] == ADD_COURSE:
                        x = {
                        "srcip": srcip,
                        "sport": sport,
                        "id" : buffer['id'],
                        "user_type" : buffer['user_type'],
                        "course_name" : buffer['course_name'],
                        "course_id" : buffer['course_id'],
                        "course_type" : buffer['course_type'],
                        "course_credits" : buffer['course_credits'],
                        'request' : buffer['request'],
                        }

                    elif buffer['request'] == DEREGISTER_COURSE:
                        x = {
                        "srcip": srcip,
                        "sport": sport,
                        "id" : buffer['id'],
                        "user_type" : buffer['user_type'],
                        "course_name" : buffer['course_name'],
                        "course_id" : buffer['course_id'],
                        "course_type" : buffer['course_type'],
                        "course_credits" : buffer['course_credits'],
                        'request' : buffer['request'],
                        }  

                    elif buffer['request'] == REGISTER_COURSE:
                        x = {
                        "srcip": srcip,
                        "sport": sport,
                        "id" : buffer['id'],
                        "user_type" : buffer['user_type'],
                        "course_name" : buffer['course_name'],
                        "course_id" : buffer['course_id'],
                        "course_type" : buffer['course_type'],
                        "course_credits" : buffer['course_credits'],
                        'request' : buffer['request'],
                        }  
                    ## This action should be handled by leader itself.
                    s2.sendto(json.dumps(x).encode(), LEADER) 
                    

                elif buffer['request_type'] == 'read':
                    if buffer['request'] == GET_MY_COURSES:
                        x = {
                        "srcip": srcip,
                        "sport": sport,
                        "id" : buffer['id'],
                        "user_type" : buffer['user_type'],
                        'request' : buffer['request'],
                        }    

                    elif buffer['request'] == VIEW_COURSE_DETAILS:
                        x = {
                        "srcip": srcip,
                        "sport": sport,
                        "id" : buffer['id'],
                        "user_type" : buffer['user_type'],
                        'request' : buffer['request'],
                        }   

                    elif buffer['request'] == GET_AVAILABLE_COURSES:
                        x = {
                        "srcip": srcip,
                        "sport": sport,
                        "id" : buffer['id'],
                        "user_type" : buffer['user_type'],
                        'request' : buffer['request'],
                        } 
                    # Forward read requests to other servers.
                    s2.sendto(json.dumps(x).encode(), select_server())
                    

        except KeyboardInterrupt:
            pass

        finally:
            print("Terminating loadbalancer...")
            s1.close()
            s2.close()