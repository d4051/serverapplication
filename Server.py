from asyncore import write
import socket
import json
from time import sleep
from pymongo import MongoClient
from pprint import pprint
from bson.json_util import dumps, loads
from multicast_sender import send_multicast,create_multicast_sender
from registration_data import update_course_collection,update_registration_collection
from multicast_sender import write_global_count

REGISTER_COURSE = "register_course"
DEREGISTER_COURSE = "deregister_course"
GET_MY_COURSES = "get_my_course"
ADD_COURSE = "add_course"
VIEW_COURSE_DETAILS = "view_course_details"
GET_AVAILABLE_COURSES = "get_available_courses"
UPDATE_COURSE_COLLECTION = "update_course_collection"
RESPONSE_OK = "SUCCESS"
RESPONSE_ERROR = "ERROR"

# connect to MongoDB
client = MongoClient(port=27017)

db=client.CourseRegistration
# Issue the serverStatus command and print the results
serverStatusResult=db.command("serverStatus")
#pprint(serverStatusResult)

def init_server():
    global server_socket
    global multicast_socket
    global total_messages
    global last_multicast_message
    global last_multicast_message_queue
    global server_address
    global server_port
    global buffer_size
    # Create a UDP socket
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Server application IP address and port
    server_address = socket.gethostbyname(socket.gethostname())
    server_port = 9999

    # Buffer size
    buffer_size = 65536

    # Message to be sent to client

    # Bind socket to port
    server_socket.bind((server_address, server_port))
    server_socket.settimeout(3.0)
    print('Server up and running at {}:{}'.format(server_address, server_port))

    multicast_socket = create_multicast_sender()
    multicast_socket.settimeout(1.0)
    total_messages = 0

    last_multicast_message_queue = []
    last_multicast_message = {}

def run_server():
    global server_socket
    global multicast_socket
    global total_messages
    global last_multicast_message
    global last_multicast_message_queue
    global server_address
    global server_port
    global buffer_size

    write_global_count(0)
    while True:
        try: 
            ## Resend multicast packet if server requests for it.
            # print('\nWaiting to receive multicast_socket message...\n')
            data, addr = multicast_socket.recvfrom(1024)
            if data:
                buffer = json.loads(data)
                if(buffer['request'] == 'missed_packet'):
                    for item in last_multicast_message_queue:
                        if(item['total_messages'] == buffer['message']):
                            print ("sending missed packet")
                            send_multicast(multicast_socket,item['buffer'],item['total_messages'])
                            last_multicast_message_queue.pop()
                            print (item)

        except socket.timeout:
            pass
        except KeyboardInterrupt:
            pass
    
        try:
            # Receive message from client
            data, address = server_socket.recvfrom(buffer_size)
            print('Received message from client: ', address)
            buffer = json.loads(data)
            client_address =(str(buffer['srcip']),int(str(buffer['sport'])))
            print(client_address)

            message = 'Hi client! Replying from server with IP ' + str(server_address) + ' and port ' + str(server_port) + '\n request : ' + buffer['request']

            # Check Request type ...getmycourse, getAchievements
            if buffer['request'] == UPDATE_COURSE_COLLECTION:
                print("Update database collection--received from serverleader")
                update_course_collection(loads(buffer['course_collection_data']))
                update_registration_collection(loads(buffer['registration_collection_data']))
                write_global_count(buffer['global_sequence_count'])

            elif buffer['request'] == ADD_COURSE:
                course = {
                "id" : buffer['id'],
                "user_type" : buffer['user_type'],
                "course_name" : buffer['course_name'],
                "course_id" : buffer['course_id'],
                "course_type" : buffer['course_type'],
                "course_credits" : buffer['course_credits'],
                "no_of_students" : 0
                }
                result=db.CourseDetails.insert_one(course)
                if result.acknowledged:
                    total_messages = total_messages +1
                    last_multicast_message = {
                        'buffer' : buffer,
                        'total_messages' : total_messages
                    }
                    last_multicast_message_queue.append(last_multicast_message)
                    send_multicast(multicast_socket,buffer,total_messages)
                    sleep(1.0)
                    server_socket.sendto(str.encode(RESPONSE_OK), client_address)

            elif buffer['request'] == VIEW_COURSE_DETAILS:
                '''
                x = {
                    "id" : buffer['id'],
                    "user_type" : buffer['user_type'],
                    'request' : buffer['request'],
                } 
                '''        
                course_details = db.CourseDetails.find({'id': buffer['id']})
                list_data = list(course_details)
                json_data = dumps(list_data)
                server_socket.sendto(str.encode(json_data), client_address)

            elif buffer['request'] == GET_MY_COURSES:
                ''' x = {
                "id" : buffer['id'],
                "user_type" : buffer['user_type'],
                'request' : buffer['request'],
                }   ''' 
                courses = db.RegistrationDetails.find({'id': buffer['id']})
                list_data = list(courses)
                json_data = dumps(list_data)
                server_socket.sendto(str.encode(json_data), client_address)

            elif buffer['request'] == GET_AVAILABLE_COURSES:
                ''' x = {
                "id" : buffer['id'],
                "user_type" : buffer['user_type'],
                'request' : buffer['request'],
                }   ''' 
                all_courses = db.CourseDetails.find()
                list_all_courses= list(all_courses)
                registered_courses = db.RegistrationDetails.find({'id': buffer['id']})
                list_registered_courses = list(registered_courses)

                for i in list_registered_courses:
                    for j in list_all_courses:
                        if(i['course_id'] == j['course_id']):
                            list_all_courses.remove(j)
                            

                json_data = dumps(list_all_courses)
                server_socket.sendto(str.encode(json_data), client_address)

            elif buffer['request'] == REGISTER_COURSE:
                course = {
                "id" : buffer['id'],
                "user_type" : buffer['user_type'],
                "course_name" : buffer['course_name'],
                "course_id" : buffer['course_id'],
                "course_type" : buffer['course_type'],
                "course_credits" : buffer['course_credits'],
                }
                result=db.RegistrationDetails.insert_one(course)
                if (result.acknowledged):
                    registered_students = db.RegistrationDetails.count_documents({'course_id': buffer['course_id']})
                
                    filter = {'course_id': buffer['course_id']}
                    newvalues = { "$set": { 'no_of_students': registered_students } }

                    # Update student count to course details.
                    db.CourseDetails.update_one(filter,newvalues)

                    
                    total_messages = total_messages +1
                    last_multicast_message = {
                        'buffer' : buffer,
                        'total_messages' : total_messages
                    }
                    last_multicast_message_queue.append(last_multicast_message)
                    send_multicast(multicast_socket,buffer,total_messages)
                    sleep(1.0)
                    server_socket.sendto(str.encode(RESPONSE_OK), client_address)
                else:
                    server_socket.sendto(str.encode(RESPONSE_ERROR), client_address)
                

            elif buffer['request'] == DEREGISTER_COURSE:
                course = {
                "id" : buffer['id'],
                "user_type" : buffer['user_type'],
                "course_name" : buffer['course_name'],
                "course_id" : buffer['course_id'],
                "course_type" : buffer['course_type'],
                "course_credits" : buffer['course_credits'],
                }
                result=db.RegistrationDetails.delete_one({'id': buffer['id'],'course_id': buffer['course_id']})

                if (result.acknowledged):
                    registered_students = db.RegistrationDetails.count_documents({'course_id': buffer['course_id']})
                
                    filter = {'course_id': buffer['course_id']}
                    newvalues = { "$set": { 'no_of_students': registered_students } }

                    # Update student count to course details.
                    db.CourseDetails.update_one(filter,newvalues)

                    total_messages = total_messages +1
                    last_multicast_message = {
                        'buffer' : buffer,
                        'total_messages' : total_messages
                    }
                    last_multicast_message_queue.append(last_multicast_message)
                    send_multicast(multicast_socket,buffer,total_messages)
                    sleep(1.0)
                    server_socket.sendto(str.encode(RESPONSE_OK), client_address)
                else:
                    server_socket.sendto(str.encode(RESPONSE_ERROR), client_address)
                    
        except socket.timeout:
            pass

        except KeyboardInterrupt:
            pass

