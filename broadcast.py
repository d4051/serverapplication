import socket 

def createBroadcastListener(BROADCAST_IP, BROADCAST_PORT, listener_socket):
    # Set the socket to broadcast and enable reusing addresses
    listener_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    listener_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # Bind socket to address and port
    listener_socket.bind((BROADCAST_IP, BROADCAST_PORT))

def broadcast(ip, port, broadcast_message):
    # Create a UDP socket
    broadcast_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # Send message on broadcast address
    broadcast_socket.sendto(str.encode(broadcast_message), (ip, port))
    # Receives response
    data, server = broadcast_socket.recvfrom(1024)
    return data.decode()
    #broadcast_socket.close()

def storeIP(content):
    # Checks for "ips.txt" where addresses are stored, creates file if it does not exist
    file_route = "ips.txt"
    file = open(file_route, "a+")
    # Appends new address to the group as a list
    file.writelines(content + "\n")
    file.close()

def readFile():
    file_route = "ips.txt"
    file = open(file_route, "r")
    ips = file.readlines()
    # Returns the existing group of addresses
    return ips
    #file.close()

def sendResponse(addr, data, socket):
    # Sends the group of addresses 
    message = str(data)
    socket.sendto(str.encode(message), addr)

def storeListenerIp(ip):
    file_route = "ips.txt"
    file = open(file_route, "a+")
    file.writelines(ip + "\n")
    file.close()

def deteleContent():
    file_route = "ips.txt"
    file = open(file_route, "r+")
    file.truncate(0)