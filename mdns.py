
import argparse
import logging
import socket
from time import sleep
import os
from zeroconf import IPVersion, ServiceInfo, Zeroconf
from random import randrange
LOADBALANCER_PORT = 10001

# info = ServiceInfo(
#         "_http._udp.local.",
#         "Course_Registration5._http._udp.local.",
#         addresses=[socket.inet_aton(socket.gethostbyname(socket.gethostname()))],
#         port=LOADBALANCER_PORT,
#         server="registration-server.local.",
# )

# parser = argparse.ArgumentParser()
# parser.add_argument('--debug', action='store_true')
# version_group = parser.add_mutually_exclusive_group()
# version_group.add_argument('--v6', action='store_true')
# version_group.add_argument('--v6-only', action='store_true')
# args = parser.parse_args()

# if args.debug:
#     logging.getLogger('zeroconf').setLevel(logging.DEBUG)
# if args.v6:
#     ip_version = IPVersion.All
# elif args.v6_only:
#     ip_version = IPVersion.V6Only
# else:
#     ip_version = IPVersion.V4Only

# zeroconf = Zeroconf(ip_version=ip_version)

def RegisterService():
    logging.basicConfig(level=logging.DEBUG)
    print("Registering Course Registration Service...")
    # zeroconf.register_service(info)

    logging.basicConfig(level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    version_group = parser.add_mutually_exclusive_group()
    version_group.add_argument('--v6', action='store_true')
    version_group.add_argument('--v6-only', action='store_true')
    args = parser.parse_args()

    if args.debug:
        logging.getLogger('zeroconf').setLevel(logging.DEBUG)
    if args.v6:
        ip_version = IPVersion.All
    elif args.v6_only:
        ip_version = IPVersion.V6Only
    else:
        ip_version = IPVersion.V4Only

    desc = {'path': '/~paulsm/'}

    service_name = "Course_Registration" + str(randrange(99)) + "._http._udp.local."
    info = ServiceInfo(
            "_http._udp.local.",
            service_name,
            addresses=[socket.inet_aton(socket.gethostbyname(socket.gethostname()))],
            port=LOADBALANCER_PORT,
            server="registration-server.local.",
    )

    zeroconf = Zeroconf(ip_version=ip_version)
    print("Registration of a service, press Ctrl-C to exit...")
    zeroconf.register_service(info)
    try:
        while True:
            sleep(0.1)
    except KeyboardInterrupt:
        pass
    finally:
        print("Unregistering Course Registration Service...")
        zeroconf.unregister_service(info)
        zeroconf.close()
    
   
def UnregisterService():   
    print("Unregistering Course Registration Service...")
    # zeroconf.unregister_service(info)
    # zeroconf.close()
