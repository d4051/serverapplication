from pymongo import MongoClient
from bson.json_util import dumps, loads

# connect to MongoDB
client = MongoClient(port=27017)

db=client.CourseRegistration

def get_course_collection():
    course_details = db.CourseDetails.find({})
    list_course_details = list(course_details)
    # print(list_course_details)
    json_data = dumps(list_course_details)
    return json_data

def get_registration_collection():
    registration_details = db.RegistrationDetails.find({})
    list_registration_details = list(registration_details)
    # print(list_registration_details)
    json_data = dumps(list_registration_details)
    return json_data

def update_course_collection(docs):
    db.CourseDetails.delete_many({})
    for d in docs:
        data = {"$set":{"id":d['id'],"user_type":d['user_type'],"course_name":d['course_name'],
        "course_id":d['course_id'],"course_type":d['course_type'],"course_credits":d['course_credits'],"no_of_students":d['no_of_students']}}
        db.CourseDetails.update_many({'_id':d['_id']}, data,True)

def update_registration_collection(docs):
    db.RegistrationDetails.delete_many({})
    for d in docs:
        data = {"$set":{"id":d['id'],"user_type":d['user_type'],"course_name":d['course_name'],"course_id":d['course_id'],
        "course_type":d['course_type'],"course_credits":d['course_credits']}}
        db.RegistrationDetails.update_many({'_id':d['_id']}, data,True)