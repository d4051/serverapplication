from os import read
import socket

def storeIP(content):
    # Checks for "ips.txt" where addresses are stored, creates file if it does not exist
    file_route = "ips.txt"
    file = open(file_route, "a+")
    # Appends new address to the group as a list
    file.writelines(content + "\n")
    file.close()

def readFile():
    file_route = "ips.txt"
    file = open(file_route, "r")
    ips = file.readlines()
    # Returns the existing group of addresses
    return ips
    #file.close()

def sendResponse(addr, data):
    # Sends the group of addresses 
    message = str(data)
    listen_socket.sendto(str.encode(message), addr)

if __name__ == '__main__':
    # Listening port
    BROADCAST_PORT = 5973

    # Local host information
    MY_HOST = socket.gethostname()
    MY_IP = socket.gethostbyname(MY_HOST)

    # Create a UDP socket
    listen_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # Set the socket to broadcast and enable reusing addresses
    listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # Bind socket to address and port
    listen_socket.bind((MY_IP, BROADCAST_PORT))

    print("Listening to broadcast messages")

    #print(MY_HOST, MY_IP)

    while True:
        data, addr = listen_socket.recvfrom(1024)
        if data:
            data_ip = data.decode()
            print("Received broadcast message:", data_ip)
            # Stores address on a file
            storeIP(data_ip)
            # Retrives the list of adresses that are stored in the file
            ips = readFile()
            print(ips)
            # Sends the list of addresses
            sendResponse(addr, ips)

